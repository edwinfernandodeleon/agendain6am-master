-- DB AgendaIn6am

CREATE DATABASE AgendaIn6am;
USE AgendaIn6am;

CREATE TABLE Usuario (
	idUsuario INT NOT NULL AUTO_INCREMENT,
    usuario VARCHAR(30) NOT NULL,
    contrasena VARCHAR(30) NOT NULL,
    PRIMARY KEY (idUsuario)
);

CREATE TABLE Categoria(
	idCategoria INT NOT NULL AUTO_INCREMENT,
    nombreCategoria VARCHAR(30) NOT NULL,
    PRIMARY KEY (idCategoria)
);

CREATE TABLE Contacto(
	idContacto INT NOT NULL auto_increment,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    direccion VARCHAR(30) NOT NULL,
    telefono VARCHAR(12) NOT NULL,
    correo VARCHAR(40) NOT NULL,
    idCategoria INT NOT NULL,
    PRIMARY KEY (idContacto),
    FOREIGN KEY (idCategoria) REFERENCES Categoria (idCategoria)
);

CREATE TABLE DetalleUsuario(
	idUsuarioDetalle INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idContacto INT NOT NULL,
    PRIMARY KEY (idUsuarioDetalle),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idContacto) REFERENCES Contacto(idContacto)
);

