var express = require('express');
var router = express.Router();

//sesion
var session = require('client-sessions');
var app = express();

var lista = [
  {"nombre": "Javier"},
  {"nombre": "Jorge"},
  {"nombre": "Fredy"},
  {"nombre": "Juan"},
  {"nombre": "Aisis"},
  {"nombre": "Henry"}
];

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/nombres', function(req, res) {
  res.render('lista', {lista});
});

router.get('/iniciar', function(req, res) {
  res.render('login', { title: 'Express'});
});

router.post('/nombre/nuevo', function(req, res) {
  
});

//para la sesión----------------------------------------
app.use(session({
  cookieName: 'session',
  secret: 'random_string_goes_here',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));

/*app . post ( '/nombres' ,   function ( req ,   res )   { 
 User . findOne ( {   email :   req . body . email   } ,   function ( err ,   user )   { 
   if   ( ! user )   { 
     res . render ( '/' ,   {   error :   'Invalid email or password.'   } ) ; 
   }   else   { 
     if   ( req . body . password   ===   user . password )   { 
       // sets a cookie with the user's info 
       req . session . user   =   user ; 
       res . redirect ( '/viewsUser/' ) ; 
     }   else   { 
       res . render ( '/' ,   {   error :   'Invalid email or password.'   } ) ; 
     } 
   } 
 } ) ; 
} ) ; */

///*----------------------------------------------------

module.exports = router;
