var express = require('express');
var router = express.Router();

/* GET home page users. */
router.get('/viewsUser/', function(req, res) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
