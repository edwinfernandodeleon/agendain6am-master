var express = require('express');
var detalleUsuario = require('../model/detalleUsuario');
var router = express.Router();

router.get('/api/detalleUsuario', function(req, res) {
  detalleUsuario.selectAll(function(error, resultados){
    if (typeof resultados !== undefined) {
      res.json(resultados);
    } else {
      res.json({"Mensaje": "No hay detalleUsuarios"});
    }
  });
});

//obtener solo un dato
router.get('/api/detalleUsuario/:idDetalleUsuario', function(req, res) {
  var idDetalleUsuario = req.params.idDetalleUsuario;

  detalleUsuario.select(idDetalleUsuario, function(error, resultado){
    if (typeof resultado !== undefined) {
      res.json(resultado);
    } else {
      res.json({"Mensaje": "No hay detalleUsuarios"});
    }
  });
});

router.post('/api/detalleUsuario', function(req, res) {
  var data = {
    idDetalleUsuario : null,
    nombreDetalleUsuario : req.body.nombreDetalleUsuario
  }

  detalleUsuario.insert(data, function(error, resultado){
    if (resultado && resultado.insertId > 0) {
      var idDetalleUsuario = resultado.insertId;
      res.redirect("api/detalleUsuario/"+idDetalleUsuario);
    } else {
      res.json({"Mensaje": "No hay detalleUsuarios"});
    }
  });
});

router.put('/api/detalleUsuario/:idDetalleUsuario' , function(req, res) {
  var idDetalleUsuario = req.params.idDetalleUsuario;
  var data = {
    idDetalleUsuario : req.body.idDetalleUsuario,
    nombreDetalleUsuario : req.body.nombreDetalleUsuario
  }

  if(idDetalleUsuario === data.idDetalleUsuario){
    detalleUsuario.update(data, function(errro, resultado){
      if(resultado !== undefined){
        res.redirect("/api/detalleUsuario/");
      } else{
        res.json({"Mensaje":"No se modificó la detalleUsuario"});
      }
    });
  }
  else{
    res.json({"Mensaje":"No concuerdan los id´s"});
  }
});

//obtener solo un dato
router.delete('/api/detalleUsuario/:idDetalleUsuario', function(req, res) {
  var idDetalleUsuario = req.params.idDetalleUsuario;

  detalleUsuario.select(idDetalleUsuario, function(error, resultado){
    if (typeof resultado !== undefined) {
      res.json(resultado);
    } else {
      res.json({"Mensaje": "No se elimino la detalleUsuario"});
    }
  });
});
module.exports = router;
