var express = require('express');
var contacto = require('../model/contacto');
var router = express.Router();

router.get('/api/contacto', function(req, res) {
  contacto.selectAll(function(error, resultados){
    if (typeof resultados !== undefined) {
      res.json(resultados);
    } else {
      res.json({"Mensaje": "No hay contactos"});
    }
  });
});

//obtener solo un dato
router.get('/api/contacto/:idContacto', function(req, res) {
  var idContacto = req.params.idContacto;

  contacto.select(idContacto, function(error, resultado){
    if (typeof resultado !== undefined) {
      res.json(resultado);
    } else {
      res.json({"Mensaje": "No hay contactos"});
    }
  });
});

router.post('/api/contacto', function(req, res) {
  var data = {
    idContacto : null,
    nombreContacto : req.body.nombreContacto
  }

  contacto.insert(data, function(error, resultado){
    if (resultado && resultado.insertId > 0) {
      var idContacto = resultado.insertId;
      res.redirect("api/contacto/"+idContacto);
    } else {
      res.json({"Mensaje": "No hay contactos"});
    }
  });
});

router.put('/api/contacto/:idContacto' , function(req, res) {
  var idContacto = req.params.idContacto;
  var data = {
    idContacto : req.body.idContacto,
    nombreContacto : req.body.nombreContacto
  }

  if(idContacto === data.idContacto){
    contacto.update(data, function(errro, resultado){
      if(resultado !== undefined){
        res.redirect("/api/contacto/");
      } else{
        res.json({"Mensaje":"No se modificó la contacto"});
      }
    });
  }
  else{
    res.json({"Mensaje":"No concuerdan los id´s"});
  }
});

//obtener solo un dato
router.delete('/api/contacto/:idContacto', function(req, res) {
  var idContacto = req.params.idContacto;

  contacto.select(idContacto, function(error, resultado){
    if (typeof resultado !== undefined) {
      res.json(resultado);
    } else {
      res.json({"Mensaje": "No se elimino la contacto"});
    }
  });
});
module.exports = router;
