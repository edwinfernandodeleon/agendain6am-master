var database = require('./database');
var detalleUsuario = {};
detalleUsuario.selectAll = function(callback) {
  if(database) {
    database.query("SELECT * FROM DetalleUsuario",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}

//este es buscar
detalleUsuario.select = function(idDetalleUsuario, callback) {
  if(database) {
    var consulta = "SELECT * FROM DetalleUsuario WHERE idDetalleUsuario = ?";
    database.query(consulta,idDetalleUsuario, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });
  }
}

detalleUsuario.insert = function(data, callback) {
  if(database) {
    database.query("INSERT INTO DetalleUsuario SET ?"),
    data, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId" : resultado.insertId});
      }
    }
  }
}

detalleUsuario.update = function (data, callback){
  if(database) {
    var sql = "UPDATE detalleUsuario SET nombreDetalleUsuario = ? WHERE idDetalleUsuario = ?";
    database,query(sql, [data.nombreDetalleUsuario, data.idDetalleUsuario],
      function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, data);
      }
    });
  }
}

detalleUsuario.delete = function(idDetalleUsuario, callback) {
  if(database) {
    var consulta = "DELETE * FROM detalleUsuario WHERE idDetalleUsuario = ?";
    database.query(consulta,idDetalleUsuario, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        var notificacion = {"Mensaje" : ""};
        if(resultado.affectedRows > 0){
          notificacion.Mensaje = "Se eliminó el DetalleUsuario";
        } else {
          notificacion.Mensaje = "No se eliminó la DetalleUsuario";
        }
         callback(null, notificacion);
      }
    });
  }
}
module.exports = detalleUsuario;
